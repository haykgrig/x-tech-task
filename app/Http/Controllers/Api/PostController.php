<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\PostShowRequest;
use App\Http\Requests\PostStoreRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\DB;


class PostController extends Controller
{

    public function index()
    {
        $posts = DB::table('posts')->get();

        return response($posts, 200);
    }

    public function store(PostStoreRequest $request, User $user)
    {
        return $request->persist($user);
    }

    public function show(PostShowRequest $request, User $user)
    {
        return $request->persist($user);
    }

}
