<?php

namespace App\Http\Requests;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\Facades\Image;

class PostStoreRequest extends FormRequest
{
    private $post;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function persist($user)
    {
        $image = $this->file('image');
        $fileName = time() . '.' . $image->getClientOriginalExtension();

        $this->post = Post::create([
            'creator_id' => $user->id,
            'description' => $this->description,
            'image' => $fileName
        ]);

        if($this->post)
        {
            Image::make($image)->resize(300, 300)->save(storage_path().'/app/public/'.$fileName);

            return $this->post;
        }

        return false;

    }
}
