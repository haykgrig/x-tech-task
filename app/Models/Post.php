<?php

namespace App\Models;

use App\Models\Relations\Post\Relations;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Relations;

    protected $fillable = ['image', 'description','creator_id'];
}
