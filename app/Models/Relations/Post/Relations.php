<?php

namespace App\Models\Relations\Post;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait Relations
{
    public function creator(): BelongsTo
    {
        return $this->BelongsTo(User::class,'id','creator_id');
    }

}
