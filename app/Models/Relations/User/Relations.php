<?php

namespace App\Models\Relations\User;

use App\Models\Post;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait Relations
{
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class,'creator_id','id');
    }

}
