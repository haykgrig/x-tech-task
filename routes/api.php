<?php

use Illuminate\Http\Request;

Route::group(['middleware' => ['json.response']], function () {

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::group(['prefix' => 'auth'], function () {

        Route::post('/login', 'Api\AuthController@login')->name('login.api');
        Route::post('/register', 'Api\AuthController@register')->name('register.api');

    });

    Route::get('newsFeed','Api\PostController@index');
    Route::post('users/{user}/posts','Api\PostController@store');
    Route::get('users/{user}/posts','Api\PostController@show');

});
